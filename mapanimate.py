import geotiler
import csv
import logging
import argparse
import collections
import matplotlib.pyplot as plt

TrackPoint = collections.namedtuple('TrackPoint', ['latitude', 'longitude', 'time', 'datetime', 'is_video'])
log = logging.getLogger('mapanimate')

def read_csv(file_name):
    log.info('Reading from "%s"', file_name)

    points = []
    with open(file_name,'r') as f:
        csv_reader = csv.DictReader(f)
        for row in csv_reader:
            points.append(TrackPoint(float(row['latitude']), float(row['longitude']), int(row['time(millisecond)']), row['datetime(utc)'], row['isVideo']=='1'))
    log.info('Read %d points from file', len(points))
    return points

def get_map_bounding(points):
    min_lat = 1000
    max_lat = -1000
    min_lon = 1000
    max_lon = -1000
    min_video = 0
    max_video = 0
    is_video = False
    for p in points:
        if p.is_video:
            if not is_video:
                is_video = True
                min_video = p.time
            max_video = p.time

        if p.latitude > max_lat:
            max_lat = p.latitude
        if p.latitude < min_lat:
            min_lat = p.latitude
        if p.longitude > max_lon:
            max_lon = p.longitude
        if p.longitude < min_lon:
            min_lon = p.longitude

    return ((min_lon, min_lat, max_lon, max_lat), (min_video, max_video))

def get_map(bounding_box):    
    log.info('Map bounding box {}'.format(bounding_box))
    map = geotiler.Map(extent=bounding_box, size=(1920,1080))
    image = geotiler.render_map(map)
    return image
    
def render_frame(points, frame_num):
    fig = plt.figure(figsize=(16,9), dpi=300)
    ax = plt.subplot(111)
    plt.axis('off')    
    
    x, y = zip(*(map.rev_geocode(p) for p in ((p.longitude, p.latitude) for p in points)))
    ax.plot(x, y, c='red',linewidth=2, alpha=0.6)
    
    f2 = ax.imshow(image)
    f2.axes.get_xaxis().set_visible(False)
    f2.axes.get_yaxis().set_visible(False)    

    plt.savefig('path.png', bbox_inches='tight', pad_inches=0)
    plt.close()

def main():    
    parser = argparse.ArgumentParser(description="Takes a CSV from AirData and renders a map to video")
    parser.add_argument('file_name', help='The file to read')
    parser.add_argument('--fps',dest='fps',default=29.97, type=float, help='FPS of output video')

    args = parser.parse_args()
    points = read_csv(args.file_name)

    bounding_box, video_times = get_map_bounding(points)

    frame_time = 1000 / args.fps
    video_time = (video_times[1]-video_times[0])
    video_frames = video_time/frame_time

    log.info('Output video at %.2f fps (%.2f ms/frame)', args.fps, frame_time)
    
    
    log.info('Seconds of video: %.2f (%d frames)', video_time/1000, video_frames)
    

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()